<?php

namespace MiamiOH\RestngIaCampus\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class CampusResourceProvider extends ResourceProvider
{

    private $tag = "IA";
    private $dot_path = "IA.Campus";
    private $s_path = "/ia/campus/v1";
    private $bs_path = '\Campus';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Campus',
            'type' => 'object',
            'properties' => array(
                'campusCode' => array('type' => 'string', 'description' => 'Name of the campus'),
                'campusDescription' => array('type' => 'string', 'description' => 'Description of the campus'),
                'groupCode' => array('type' => 'string', 'description' => 'Code for the group the campus belongs to'),
                'groupName' => array('type' => 'string', 'description' => 'Name of the group the campus belongs to'),
                'classificationCode' => array('type' => 'string', 'description' => 'Oxford or Regionals'),
                'classificationDescription' => array('type' => 'string', 'description' => 'Oxford or Regionals'),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Campus.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Campus',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Campus',
            'class' => 'MiamiOH\RestngIaCampus\Services' . $this->bs_path,
            'description' => 'This service provides resources about campuses from IA.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'Return all Campuses',
                'pattern' => $this->s_path,
                'service' => 'Campus',
                'method' => 'getCampuses',
                'isPageable' => false,
                'tags' => array($this->tag),
                'returnType' => 'collection',
                'options' => array(
                    'campusCode' => array('type' => 'single', 'description' => 'Campus code to filter by'),
                ),
//        'middleware' => array(
//            'authenticate' => array('type' => 'token'),
//            'authorize' => array(
//                array('type' => 'authMan',
//                    'application' => 'WebServices',
//                    'module' => 'FacultyElection',
//                    'key' => array('view')
//                )
//            )
//        ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of Campuses',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Campus.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}