<?php

/*
	-----------------------------------------------------------
	FILE NAME: Campus.class.php

	Copyright (c) 2016 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Erin Mills

	DESCRIPTION:

	INPUT:
	PARAMETERS:

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:
	

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	05/27/2016		millse
	Description:	Initial Program
	
 */

namespace MiamiOH\RestngIaCampus\Services;

class Campus extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_GEN_IAPROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    // GET(read/view) the Ballot Information
    public function getCampuses()
    {

        //log
        $this->log->debug('Campus service was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $campusCode = null;
        $payload = array();

        //get a database handle for the IA database
        $dbh = $this->database->getHandle($this->datasource_name);

        if (isset($options['campusCode'])) {

            //if the campusCode is empty, throw an error
            if ($options['campusCode'] == '') {
                throw new \Exception('Error: campusCode cannot be empty');
            }

            //if the campusCode is malicious/invalid, throw an error
            //if(preg_match('/[^A-Za-z0-9() :\-_"\']/', $options['campusCode'])){
            if (preg_match('/[^A-Z() :\-_"\']/', $options['campusCode'])) {
                throw new \Exception('Error: campusCode is invalid');
            }

            $campusCode = $options['campusCode'];
        }

        $queryString = 'select distinct CAMPUS_CD, CAMPUS_DESC, CAMPUS_GROUP_CD, CAMPUS_GROUP_NM, CAMPUS_OXFORD_RGNL_CLS_CD, CAMPUS_OXFORD_RGNL_CLS_DESC from Mudwmgr.dim_campus
           where campus_cd is not null';

        if ($campusCode) {//if there is a campusCode filter to apply, add it.
            $queryString = $queryString . ' and campus_cd = ?';
        }

        $queryString = $queryString . ' order by campus_desc';

        if ($campusCode) {
            $results = $dbh->queryall_array($queryString, $campusCode);
        } else {
            $results = $dbh->queryall_array($queryString);
        }


        //put all the results into the payload
        $payload = $this->campusMakeover($results);

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;

    }

    private function campusMakeover($campuses)
    {
        $prettycampuses = array();

        foreach ($campuses as $d) {
            $prettyD = array();
            $prettyD['campusCode'] = $d['campus_cd'];
            $prettyD['campusDescription'] = $d['campus_desc'];
            $prettyD['groupCode'] = $d['campus_group_cd'];
            $prettyD['groupName'] = $d['campus_group_nm'];
            $prettyD['classificationCode'] = $d['campus_oxford_rgnl_cls_cd'];
            $prettyD['classificationDescription'] = $d['campus_oxford_rgnl_cls_desc'];
            $prettyCampuses[] = $prettyD;
            //$prettyCampuses[] = $d;
        }

        return $prettyCampuses;
    }

}
