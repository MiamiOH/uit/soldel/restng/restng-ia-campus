<?php

/*
-----------------------------------------------------------
FILE NAME: getCampusTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:

ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

05/31/2016               millse
Description:       Initial Draft
			 
-----------------------------------------------------------
*/

namespace MiamiOH\RestngIaCampus\Tests\Unit;


class GetCampusTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $user, $ballot, $queryallRecords;


    // set up method automatically called by PHPUnit before every test method:

    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->campus = new \MiamiOH\RestngIaCampus\Services\Campus();
        $this->campus->setDatabase($db);
        $this->campus->getLogger();
        $this->campus->setDatasource($ds);
        $this->campus->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/

    /*
      *   Tests Case in which election Id option is not specified.
      *   Actual: No Election ID option specified.
      *	  Expected Return: At least one election ID must be specified.
      */
    public function testNoFilter()
    {

        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllCampuses')));

        $response = $this->campus->getCampuses();
        $this->assertEquals($this->mockExpectedAllCampusesResponse(), $response->getPayload());

    }

    public function mockExpectedAllCampusesResponse()
    {
        $expectedReturn =
            array(
                array(
                    'campusCode' => 'H',
                    'campusDescription' => 'Hamilton',
                    'groupCode' => 'H',
                    'groupName' => 'Hamilton',
                    'classificationCode' => 'R',
                    'classificationDescription' => 'Regional',
                ),
                array(
                    'campusCode' => 'P',
                    'campusDescription' => 'Hamilton Upper Level',
                    'groupCode' => 'H',
                    'groupName' => 'Hamilton',
                    'classificationCode' => 'R',
                    'classificationDescription' => 'Regional',
                ),
                array(
                    'campusCode' => 'L',
                    'campusDescription' => 'Luxembourg',
                    'groupCode' => 'O',
                    'groupName' => 'Oxford',
                    'classificationCode' => 'O',
                    'classificationDescription' => 'Oxford',
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllCampuses()
    {
        return array(
            array(
                'campus_oxford_rgnl_cls_desc' => 'Regional',
                'campus_oxford_rgnl_cls_cd' => 'R',
                'campus_group_nm' => 'Hamilton',
                'campus_group_cd' => 'H',
                'campus_desc' => 'Hamilton',
                'campus_cd' => 'H',
            ),
            array(
                'campus_oxford_rgnl_cls_desc' => 'Regional',
                'campus_oxford_rgnl_cls_cd' => 'R',
                'campus_group_nm' => 'Hamilton',
                'campus_group_cd' => 'H',
                'campus_desc' => 'Hamilton Upper Level',
                'campus_cd' => 'P',
            ),
            array(
                'campus_oxford_rgnl_cls_desc' => 'Oxford',
                'campus_oxford_rgnl_cls_cd' => 'O',
                'campus_group_nm' => 'Oxford',
                'campus_group_cd' => 'O',
                'campus_desc' => 'Luxembourg',
                'campus_cd' => 'L',
            ),
        );
    }

    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function testCampusCodeFilter()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsCampusFilter')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryCampusFilter')));

        $response = $this->campus->getCampuses();
        $this->assertEquals($this->mockExpectedCampusFilterResults(), $response->getPayload());
    }

    public function mockOptionsCampusFilter()
    {
        return array(
            'campusCode' => 'L'
        );
    }

    public function mockQueryCampusFilter()
    {
        return array(
            array(
                'campus_oxford_rgnl_cls_desc' => 'Oxford',
                'campus_oxford_rgnl_cls_cd' => 'O',
                'campus_group_nm' => 'Oxford',
                'campus_group_cd' => 'O',
                'campus_desc' => 'Luxembourg',
                'campus_cd' => 'L',
            )
        );
    }

    public function mockExpectedCampusFilterResults()
    {
        $expectedReturn =
            array(
                array(
                    'campusCode' => 'L',
                    'campusDescription' => 'Luxembourg',
                    'groupCode' => 'O',
                    'groupName' => 'Oxford',
                    'classificationCode' => 'O',
                    'classificationDescription' => 'Oxford',
                )
            );
        return $expectedReturn;
    }

    public function testCampusCodeFilterEmpty()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsCampusFilterEmpty')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $response = $this->campus->getCampuses();
            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: campusCode cannot be empty', $e->getMessage());
        }
    }

    public function mockOptionsCampusFilterEmpty()
    {
        return array(
            'campusCode' => ''
        );
    }

    public function testCampusCodeFilterInvalid()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsCampusFilterInvalid')));

        $this->request->method('getResourceParam')
            ->with($this->anything())
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        try {
            $response = $this->campus->getCampuses();
            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: campusCode is invalid', $e->getMessage());
        }
    }

    public function mockOptionsCampusFilterInvalid()
    {
        return array(
            'campusCode' => 'delete * from all_tables;'
        );
    }

}